import React from 'react'
import "../Style/Footer.css"
import AOS from "aos";

export default function Footer() {
    AOS.init({duration: 650, once:true});
  return (
    <div  data-aos="fade-down">
    <div className='ft'>
        <div className='footer1'>
        <div className="contact" style={{textAlign: "left"}}>
            <h3>Contact</h3>
        <li><i class="fas fa-phone-volume"></i> : +62 864305xxxx</li>
        <li><i class="fas fa-envelope"></i>: fatiya@gmail.com</li>
        <li><i class="fas fa-map-marker-alt"></i>: Jawa Tengah Kaliwungu</li>
        </div>
        <div className="footer" style={{display:"block"}}>
            <h3>Follow Me</h3>
        <div className='icon'>
        <a href="https://github.com/fatiyasalsabila123/project-akhir-react" target="_blank" rel="noopener noreferrer"><i style={{backgroundColor:"black", color:"white", padding:"60%"}} class="fab fa-github"></i></a>
        <a href="https://www.youtube.com/" target="_blank" rel="noopener noreferrer"><i style={{backgroundColor:"black", color:"white", padding:"50%"}} class="fab fa-youtube"></i></a>
        <a href="https://gitlab.com/" target="_blank" rel="noopener noreferrer"><i style={{backgroundColor:"black", color:"white", padding:"60%"}} class="fab fa-gitlab"></i></a>
        <a href="https://web.telegram.org/k/#@kelasbootcamp2022" target="_blank" rel="noopener noreferrer"><i style={{backgroundColor:"black", color:"white", padding:"60%"}} class="fab fa-telegram"></i></a>
        <a href="https://www.instagram.com/?hl=id" target="_blank" rel="noopener noreferrer"><i style={{backgroundColor:"black", color:"white", padding:"70%"}} class="fab fa-instagram"></i></a>
        </div>
        </div>
        <div className="about">
            <h3>
                About
            </h3>
            <p>Website Sederhana Toko Online </p>
        </div>
        </div>
        </div>
        <div className='ground'>
        <div className="copy">
            <p style={{fontWeight: "bold"}}><i class="fas fa-copyright "></i>CopyRight fatiya.com</p>
        </div>
        </div>
    </div>
  )
}
