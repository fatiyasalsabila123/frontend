import React, { useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";

const Edit = () => {
  //const adalah singkatan dari constant, yang berarti data dari variabel tidak boleh berubah dan harus selalu konstan.
  const param = useParams();
  const [gambar, setGambar] = useState("");
  const [namaMakanans, setnamaMakanans] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const history = useHistory();

  //useEffect digunakan untuk menambahkan side effect ke function komponen
  useEffect(() => {
    axios
      //get untuk menampilkan data
      .get("http://localhost:5000/product/" + param.id)
      .then((response) => {
        const makanans = response.data.data;
        setGambar(makanans.gambar);
        setnamaMakanans(makanans.namaMakanans);
        setDeskripsi(makanans.deskripsi);
        setHarga(makanans.harga);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan Sir!" + error);
      });
  }, []);

  const submitActionHandler = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
      formData.append("file", gambar);
      formData.append("namaMakanans", namaMakanans);
      formData.append("deskripsi", deskripsi);
      formData.append("harga", harga);

    await Swal.fire({
      title: "Apakah Anda Yakin Untuk Di Edit?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes!",
    })
      .then((result) => {
        if (result.isConfirmed) {
          axios
            //mengedit data
           .put("http://localhost:5000/product/"+ param.id, formData , )
        }
      })
      .then(() => {
        Swal.fire({
          title: "Berhasil",
          icon: "success",
          showConfirmButton : false,
          timer: 1500
        })
        history.push("/dashboard");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
        console.log(error);
      });
  };

  return (
    <div>
      <div className="edit">
        <div className="edit mx-5">
          <div className="container my-5">
            <Form onSubmit={submitActionHandler}>
              <div className="name mb-3">
                <Form.Label>
                  <strong>Gambar</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    type="file"
                    // value={gambar}
                    onChange={(e) => setGambar(e.target.files[0])}
                    required
                  />
                </InputGroup>
              </div> 

              <div className="place-of-birth mb-3">
                <Form.Label>
                  <strong>Nama Makanan</strong>
                </Form.Label>
                <InputGroup className="flex gap-3">
                  <Form.Control
                    value={namaMakanans}
                    onChange={(e) => setnamaMakanans(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Deskripsi</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="text"
                    value={deskripsi}
                    onChange={(e) => setDeskripsi(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Harga</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="number"
                    value={harga}
                    onChange={(e) => setHarga(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="d-flex justify-content-end align-item-center mt-2">
                <button className="buton btn" type="submit">
                  Save
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Edit;
