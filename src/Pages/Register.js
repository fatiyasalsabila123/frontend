// import axios from "axios";
import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
// import Swal from "sweetalert2";
import "../Style/Register.css"
export default function Register() {
  const history = useHistory();
  const [photoProfile, setPhotoProfile] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [email, setEmail] = useState("");
  const [noHp, setNoHp] = useState("");
  const [password, setPassword] = useState("");
  // conns [password, setPassword] =  useState();
  const [userRegister, setUserRegister] = useState({
    nama: "",
    alamat: "",
    email: "",
    noHp: "",
    password: "",
    photoProfile: null,
    role: "USER",
  });

  const register = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", photoProfile);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("email", email);
    formData.append("noHp", noHp);
    formData.append("password", password);
    formData.append("role", "USER");

    // const handleOnChange = (e) => {
    //   setUserRegister((currUser) => {
    //     return { ...currUser,[e.target.id]: e.target.value}
    //   })
    // }

    try {
      await axios.post("http://localhost:5000/users/sign-up", formData);
      alert("register succes");
      setTimeout(() => {
        history.push("/login");
      }, 1250);
    } catch (err) {
      console.log(err);
    }
  };
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");

  // const data = {
  //   email: email,
  //   password: password,
  // };

  // const history = useHistory();

  // const register = async (e) => {
  //   e.preventDefault();
  //   await axios.post("http://localhost:8000/pembelis", {
  //     email: email,
  //     password: password,
  //   });
  //   Swal.fire("Succes!", "Berhasil Melakukan Registrasi", "success")
  //     .then(() => {
  //       history.push("/dashboard");
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan " + error);
  //     });
  // };

  return (
    <div className="register3">
      <div className="register">
        <div className="container border pt-5 px-5 register1">
          <h1 className="mb-5 Users12">Form Register</h1>
          <Form onSubmit={register} method="POST">
          <div className="mb-3">
            <Form.Label>
              <strong>Email</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </InputGroup>
          </div>
            <br />
            <div className="mb-3">
            <Form.Label>
              <strong>Username</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Username"
                type="text"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                required
              />
            </InputGroup>
          </div>
            <br />
            <div className="mb-3">
            <Form.Label>
              <strong>Alamat</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Alamat"
                type="text"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
                required
              />
            </InputGroup>
          </div>
            <br />
            <div className="mb-3">
            <Form.Label>
              <strong>No Hnadphone</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="No Handphone"
                type="number"
                value={noHp}
                onChange={(e) => setNoHp(e.target.value)}
                required
              />
            </InputGroup>
          </div>
            <br />
            <div className="mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </InputGroup>
          </div>
            <br />
          <div className="mb-3">
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="file"
                onChange={(e) => setPhotoProfile(e.target.files[0])}
                required
              />
            </InputGroup>
          </div>
            <br />
            <button variant="primary" type="submit" className="mx-1 buton btn">
              Register
            </button>
            <p>
              <a href="/login">Login </a>Jika Sudah Punya Akun{" "}
            </p>
          </Form>
        </div>
      </div>
    </div>
  );
}
