import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Navigation from "../Component/Navigation";
import TambahProfile from "../Component/TambahProfile";
import "../Style/Profile.css";

export default function ProfileUser() {
  const param = useParams();
  const [profile, setProfile] = useState({
    nama: "",
    alamat: "",
    email: "",
    noHp: "",
    password: "",
    photoProfile: null,
  });
  const [photoProfile, setPhotoProfile] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [email, setEmail] = useState("");
  const [noHp, setNoHp] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  console.log(profile);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useHistory();

  // const addProfile = async (e) => {
  //   e.preventDefault();
  //   e.persist();

  //   const formData = new FormData();
  //   formData.append("photoProfile", photoProfile);
  //   formData.append("nama", nama);
  //   formData.append("alamat", alamat);
  //   formData.append("email", email);
  //   formData.append("no_hp", noHp);
  //   formData.append("password", password);
  //   try {
  //library opensource yang di gunakan untuk request data melalui http
  // await axios.post(
  //   "http://localhost:5000/users/sign-up",
  //   formData,
  ////headers berikut berfungsi untuk method yang hanya di akses oleh admin
  //   {headers: {
  //     Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     "Content-Type": "multipart/form-data",
  //   },
  // }
  // );
  //Sweet Alert
  //     setShow(false);
  //     Swal.fire({
  //       icon: "success",
  //       showConfirmButton: false,
  //       timer: 1500,
  //     });
  //     setTimeout(() => {
  //       window.location.reload();
  //     }, 1500);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // useEffect(() => {
  //    axios
  //    .get ("http://localhost:5000/users/" + localStorage.getItem("id"))
  //   .then((response) => {
  //     const poto = response.data.data;
  //     setPhotoProfile(poto.photoProfile);
  //     setNama(poto.nama);
  //     setAlamat(poto.alamat);
  //     setEmail(poto.alamat);
  //     setNoHp(poto.noHp);
  //     setPassword(poto.password);
  //   })
  //   .catch((error) => {
  //     alert("Terjadi kesalahan " + error);
  //   })
  // }, [0])

  const getAll = async () => {
    await axios
      .get("http://localhost:5000/users/" + localStorage.getItem("id"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", photoProfile);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("email", email);
    formData.append("noHp", noHp);
    formData.append("password", password);

    try {
      await axios.put(
        `http://localhost:5000/users/${localStorage.getItem("id")}`,
        formData
      );
      window.location.reload();
    } catch (err) {
      console.log(err);
    }
  };
  //abc
  //   await Swal.fire({
  //       title: "Apakah Anda Yakin Untuk Di Edit?",
  //   icon: "warning",
  //   showCancelButton: true,
  //   confirmButtonColor: "#3085d6",
  //   cancelButtonColor: "#d33",
  //   confirmButtonText: "Yes!",
  // })
  //   .then((res) => {
  //       if (res.isConfirmed)
  //       axios.put(`http://localhost:5000/users/${localStorage.getItem("id")}`, formData );
  //     Swal.fire({
  //         title: "Berhasil",
  //         icon: "success",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       setTimeout(() => {
  //           // window.location.reload();
  //         }, 1000);
  //       })
  //       .catch((error) => {
  //           alert("Terjadi Kesalahan " + error);
  //           console.log(error);
  //         });
  //       getAll();

  useEffect(() => {
    axios
      .get("http://localhost:5000/users/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setPhotoProfile(profile.photoProfile);
        setNama(profil.nama);
        setAlamat(profil.alamat);
        setEmail(profil.email);
        setNoHp(profil.noHp);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  // const submitActionHandler = async (e) => {
  //   e.preventDefault();
  //   await Swal.fire({
  //     title: "Apakah Anda Yakin Untuk Di Edit?",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Yes!",
  //   })
  //     .then((result) => {
  //       if (result.isConfirmed) {
  //         axios.put(
  //           "http://localhost:5000/users/" + localStorage.getItem("id"),
  // {
  //   headers: {
  //     Authorization: `Bearer ${localStorage.getItem("token")}`,
  //   },
  // }
  //           {
  //             photoProfile: photoProfile,
  //             nama: nama,
  //             alamat: alamat,
  //             email: email,
  //             noHp: noHp,
  //             password: password,
  //           }
  //         );
  //       }
  //     })
  //     .then(() => {
  //       Swal.fire({
  //         title: "Berhasil",
  //         icon: "success",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       history.push("/profile");
  //       setTimeout(() => {
  //         // window.location.reload();
  //       }, 1000);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan " + error);
  //       console.log(error);
  //     });
  // };

  return (
    <div>
      <Navigation />
      <Modal show={show} onHide={handleClose} >
        <Modal.Header closeButton style={{ backgroundColor: "#5F9DF7" }}>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body className="form">
          <Form onSubmit={putProfile}>
            <div className="mb-3">
              <Form.Label>
                <strong>Photo Profile</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="file"
                  // value={photoProfile}
                  onChange={(e) => setPhotoProfile(e.target.files[0])}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Username</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukan Username"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukan Alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="email"
                  placeholder="Masukan Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>No Handphone</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="number"
                  placeholder="Masukan Nomor Handphone"
                  value={noHp}
                  onChange={(e) => setNoHp(e.target.value)}
                  required className="number1"
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukan Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <Button
              variant="danger"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <div  className="css1" onSubmit={putProfile} >
        <div className="tulisan1">
      <h1 className="profile2">Profile <span className="saya1">Saya</span></h1>
      <h3 className="kelola">Kelola informasi profil Anda untuk mengontrol, melindungi dan mengamankan akun</h3>
      </div>
        <div className="css">
          <img
            className="pmd"
            src="https://png.pngtree.com/thumb_back/fw800/background/20190830/pngtree-background-of-a-sunset-landscape-in-the-rocky-mountains-image_309847.jpg"
            alt=""
          />
          {<img className="gambar1" src={profile.photoProfile} alt="" />}
          <br />
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "10px",
              marginBottom: "10px",
            }}
          >
            <div className="isi">
              <div className="content2">
                <div className="username1">
                  <h6 className="nama1">Username : </h6>
                  <h6 className="nama2">{profile.nama}</h6>
                </div>
                <div className="username2">
                  <h6 className="nama1">Alamat : </h6>
                  <h6 className="nama2">{profile.alamat}</h6>
                </div>
                <div className="username2">
                  <h6 className="nama1">Email : </h6>
                  <h6 className="nama2">{profile.email}</h6>
                </div>
                <div className="username2">
                  <h6 className="nama1">No Handphone : </h6>
                  <h6 className="nama2">{profile.noHp}</h6>
                </div>
                <button type="button" className="simpan" onClick={handleShow}>
                  Edit Profile
                </button>
              </div>
            </div>
            {/* <Form>
            <h3 className="profile">Profile Saya</h3>
            <hr />
            <div className="form-floating inputan1 ">
            
            <div className="form-floating noHp">
              <p>Username: </p>
              <p> {profile.nama}</p>
            </div>
            <br />
            <div className="form-floating noHp">
            <p>Alamat : </p>
              <p> {profile.alamat}</p>
            </div>
            <br />
            <div className="form-floating noHp">
              <p>Email : </p>
              <p> {profile.email}</p>
              </div>
              <br />
              <div className="form-floating noHp">
              <p>No Handphone : </p>
              <p>{profile.noHp}</p>
              </div>
              <br />
            </div>
            <br />
            <br />
           
          </Form> */}
          </div>
        </div>
      </div>
    </div>
  );
}
