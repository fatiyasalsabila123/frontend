import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';
import { Form, InputGroup } from 'react-bootstrap';
import "../Style/Login.css"

export default function LoginPembeli() {

    const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    await axios.get("http://localhost:8000/pembelis").then(({ data }) => {
      const admin = data.find(
        (x) => x.email === email && x.password === password
      );
      if (admin) {
        Swal.fire({
          icon: "success",
          title: "Success Login Sebagai " + email,
          showConfirmButton: false,
        });
        localStorage.setItem("id", admin.id);
        history.push("/dashboard");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      } else {
        Swal.fire({
          icon: "error",
          ritle: "email atau password tidak valid",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  return (
    <div>
        <div className="container border pt-3 pb-5 px-5 login">
        <h1 className="mb-5 admin">Form Login Pembeli</h1>
        <Form onSubmit={login} method="POST">
          <div className="mb-3">
            <Form.Label>
              <strong>email</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <div className="mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <button variant="primary" type="submit" className="mx-1 buton btn">
            Login
          </button>
          <p>Login Sebagai <a href="/login">Admin</a></p>
          <p><a href="/">Register</a> Jika Belum Punya Akun</p>
        </Form>
      </div>
    </div>
  )
}
