import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../Style/Login.css";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:5000/users/sign-in",
        {
          email: email,
          password: password,
        }
      );
      //jika repon status 200/ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil !!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.users.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.users.role);
        history.push("/dashboard");
        //Untuk mereload
        window.location.reload();
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }

    // await axios.get("http://localhost:8000/admin").then(({ data }) => {
    //   const admin = data.find(
    //     (x) => x.username === username && x.password === password
    //   );
    //   if (admin) {
    //     Swal.fire({
    //       icon: "success",
    //       title: "Success Login Sebagai " + username,
    //       showConfirmButton: false,
    //     });
    //     localStorage.setItem("username", admin.username);
    //     localStorage.setItem("id", admin.id);
    //     history.push("/dashboard");
    //     setTimeout(() => {
    //       window.location.reload();
    //     }, 1000);
    //   } else {
    //     Swal.fire({
    //       icon: "error",
    //       ritle: "username atau password tidak valid",
    //       showConfirmButton: false,
    //       timer: 1500,
    //     });
    //   }
    // });
  };

  return (
    <div className="login2">
      <div className="container border  pt-3 pb-5 px-5 login">
        <h1 className="mb-5 admin">Form Login</h1>
        <Form onSubmit={login} method="POST">
          <div className="mb-3">
            <Form.Label>
              <strong>Email atau Username</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <div className="mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <button variant="primary" type="submit" className="mx-1 buton btn">
            Login
          </button>
          <p>
           Jika belum punya akun silahkan <a href="/"> register </a> terlebih dahulu
          </p>
        </Form>
      </div>
    </div>
  );
}
