import React, { useEffect } from "react";
import { Carousel, Table } from "react-bootstrap";
import "../Style/Dashboard.css";
import Card from "../Component/Card";
import Tabel from "../Component/Tabel";
import List from "./List";
import Footer from "../Pages/Footer";
import AOS from "aos";
import Navigation from "./Navigation";
import { useState } from "react";
import ReactPaginate from "react-paginate";

export default function Dashboard() {

  AOS.init({ duration: 650, once: true });
  return (
    <div>
      <Navigation />
      <div className="das">
        <div
          data-aos="fade-up fta"
          id="carouselExampleIndicators"
          className="carousel slide media"
          data-bs-ride="true"
          style={{ padding: "2%" }}
        >
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div className="carousel-inner board">
            <div className="carousel-item active">
              <img
                src="https://jago.com/images/promo/thr/promo-ramadan-hangry-id.jpg"
                className="d-block w-100 gmb"
                // style={{ height: "550px" }}
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://lelogama.go-jek.com/post_featured_image/promo-ramadhan-go-food-extension.jpg"
                className="d-block w-100 gmb"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://cdn.mallofindonesia.com/wp-content/uploads/2021/01/WhatsApp-Image-2021-01-09-at-07.31.52.jpeg"
                className="d-block w-100 gmb"
                alt="..."
              />
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>

        {/* <div>
  <Images data={images}/>
</div> */}
        <Tabel />
        <Card />
        {/* <nav aria-label="Page navigation example" style={{marginLeft:"47%"}}>
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="/list">1</a></li>
    <li class="page-item"><a class="page-link" href="/tabel">2</a></li>
    <li class="page-item"><a class="page-link" href="/card">3</a></li>
    <li class="page-item"><a class="page-link" href="/dashboard">4</a></li>
  </ul>
</nav> */}
        <Footer />
      </div>
    </div>
  );
}
