import React, { useEffect, useState } from "react";
import axios from "axios";
import Navigation from "./Navigation";
import Swal from "sweetalert2";
import "../Style/Cart.css";
import ReactPaginate from "react-paginate";

export default function Cart() {
  const [makanan, setMakanan] = useState([]);
  const [totalHarga, settotalHarga] = useState(0);
  const [quantity, setQuantity] = useState(0);
  const [pages, setPages] = useState(0);
  const total = makanan.reduce((a, b) => a + b.harga, 0);

  const getAll = async (page = 0) => {
    const res = await axios.get(
      `http://localhost:5000/cart?page=${page}&user_id=${localStorage.getItem(
        "id"
      )}`
    );
    setMakanan(
      res.data.content.map((item) => ({
        ...item,
        checked: false,
      }))
    );
    let total = 0;
    res.data.content.forEach((item) => {
      total += item.totalharga;
    });
    settotalHarga(total);
    setPages(res.data.totalPages);
  };

  const rupiah = (angka) => {
    return new Intl.NumberFormat("id=ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(angka);
  };

  const deleteMakanan = async (id) => {
    Swal.fire({
      title: " apakah yakin pesanan mau di Hapus?",
      text: "Pesanan Akan Di Hapus",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus Sekarang!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:5000/cart/" + id);
        Swal.fire("Hapus!", "Berhasil Di Hapus.", "success");
        getAll();
      }
    });
  };

  const addcheckout = async () => {
    await axios.delete(`http://localhost:5000/cart/deleteAll?user_id=${localStorage.getItem("id")}`);
     Swal.fire("Succes!", "Berhasil dibeli", "success")
     getAll();
  };

  const increment = () => {
    setQuantity(quantity + 1);
  };

  const decrement = () => {
    let value = 0;
    if (quantity <= value) {
      value = 1;
    } else {
      value = quantity;
    }

    setQuantity(value - 1);
  };
  useEffect(() => {
    getAll(0);
  }, []);

  return (
    <div >
      <Navigation />
      <div className="cart2">
      <table className="table table-striped-columns responsive-3">
        <thead style={{ backgroundColor: "#FF7000" }}>
          <tr>
            <th style={{ width: "10px" }}>No</th>
            <th style={{ width: "25%" }}>Gambar</th>
            <th style={{ width: "10%" }}>Nama Makanan</th>
            <th style={{ width: "30%" }}>Deskripsi</th>
            <th style={{ width: "5px" }}>Harga</th>
            <th style={{ width: "5px" }}>Action</th>
          </tr>
        </thead>
        <tbody>
          {makanan.map((foods, index) => {
            return (
              <tr key={foods.id}>
                <td className="align-middle">{index + 1}</td>
                <td className="align-middle">
                  <img
                    style={{ width: "40%" }}
                    src={foods.productId.gambar}
                    alt="img"
                  />
                </td>
                <td className="align-middle">{foods.productId.namaMakanans}</td>
                <td className="align-middle">{foods.productId.deskripsi}</td>
                <td className="align-middle">Rp. {foods.totalharga}</td>
                <td className="align-middle action">
                  <button
                    type="button"
                    className="btn btn-danger"
                    style={{
                      color: "white",
                      border: "none",
                      padding: "1%",
                      paddingLeft: "10%",
                      paddingRight: "10%",
                      borderRadius: "5px",
                    }}
                    onClick={() => deleteMakanan(foods.id)}
                  >
                    <i class="fas fa-trash-alt"></i>
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="cek">
        <div>
          <strong className="strong"> Total Harga : Rp. {totalHarga}</strong>
        </div>
        <div>
          <button className="btn btn-success sukses" onClick={() => addcheckout()}>checkout</button>
        </div>
      </div>
      <ReactPaginate
        previousLabel={"<<"}
        nextLabel={">>"}
        breakLabel={"..."}
        pageCount={pages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={(e) => getAll(e.selected)}
        containerClassName={"pagination justify-content-center"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextClassName={"page-item"}
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />
      </div>
    </div>
  );
}
