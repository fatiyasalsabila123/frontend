import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import "../Style/Table.css";
import AOS from "aos";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function Tabel() {
  // const [makanan, setMakanan] = useState([]);
  const [list, setList] = useState([]);

  const [show, setShow] = useState(false);

  const [gambar, setGambar] = useState("");
  const [namaMakanans, setnamaMakanans] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");
  const [pages, setPages] = useState(0);
  // const [description, setDescription] = useState("")

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useHistory();

  const addList = async (e) => {
    // agar tidak reload tetapi berlaku hanya 1 file
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", gambar);
    formData.append("namaMakanans", namaMakanans);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    // formData.append("description", description)

    // const data = {
    //   gambar: gambar,
    //   namaMakanans: namaMakanans,
    //   deskripsi: deskripsi,
    //   harga: Number (harga),
    // };
    // Axios Untuk melakukan request GET
    // await axios.post("http://localhost:8000/makanans", data);
    // Swal.fire("Succes!", "Berhasil Menambahkan Menu Makanan", "success")
    //   .then(() => {
    //     window.location.reload();
    //   })
    //   .catch((error) => {
    //     alert("Terjadi Kesalahan " + error);
    //   });

    //try catch untuk memastikan terjadi masalah
    try {
      //library opensource yang di gunakan untuk request data melalui http
      await axios.post(
        "http://localhost:5000/product",
        formData,
        ////headers berikut berfungsi untuk method yang hanya di akses oleh admin
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );
      //Sweet Alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Menambahkan data !!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async (idx) => {
    await axios
      .get(
        `http://localhost:5000/product/all?query=${namaMakanans}&page=${idx}`
      )
      .then((res) => {
        setList(res.data.data.content);
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0); 
  }, []);

  const deleteMakanan = async (id) => {
    Swal.fire({
      title: " apakah yakin data mau di delete?",
      text: "Data Akan Di Hapus",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:5000/product/" + id);
        Swal.fire("Deleted!", "Data Berhasil Di Hapus", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    });
    getAll(0);
  };

  AOS.init({ duration: 650, once: true });
  return (
    <div className="tabel">
      {localStorage.getItem("role") === "ADMIN" ? (
        <>
          <div className="tambah">
            <button
              button
              type="button"
              className="btn btn-primary menu"
              onClick={handleShow}
              style={{
                padding: "5px",
                border: "none",
                fontSize: "15px",
                marginBottom: "3%",
                marginLeft: "90%",
              }}
            >
              Tambah Menu
            </button>
          </div>
          {/* <form class="d-flex" role="search">
            <input
              class="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={namaMakanans}
              onChange={(e) => setnamaMakanans(e.target.value)}
            />
            <button
              class="btn btn-outline-success"
              type="button"
              onClick={() => getAll(0)}
            >
              Search
            </button>
          </form> */}
          <br />
        </>
      ) : (
        <></>
      )}
      {localStorage.getItem("role") === "ADMIN" ? (
        <>
          <table className="table table-striped-columns responsive-3">
            <thead data-aos="fade-left" style={{ backgroundColor: "#FF7000" }}>
              <tr>
                <th style={{ width: "10px" }}>No</th>
                <th style={{ width: "150px" }}>Nama Makanan</th>
                <th style={{ width: "700px" }}>Deskripsi</th>
                <th style={{ width: "100px" }}>Harga</th>
                <th style={{ width: "150px" }}>Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map((food, index) => {
                return (
                  <tr data-aos="fade-left" key={food.id}>
                    <td>{index + 1}</td>
                    <td>{food.namaMakanans}</td>
                    <td>{food.deskripsi}</td>
                    <td>{food.harga}</td>
                    <td className="action">
                      <a href={"/edit/" + food.id}>
                        <button
                          variant="warning"
                          className="mx-1"
                          style={{
                            backgroundColor: "orange",
                            color: "white",
                            border: "none",
                            padding: "1%",
                            paddingLeft: "10%",
                            paddingRight: "10%",
                            borderRadius: "5px",
                          }}
                        >
                          <i class="fas fa-edit"></i>
                        </button>
                      </a>
                      ||{" "}
                      <button
                        type="button"
                        className="btn btn-danger"
                        style={{
                          color: "white",
                          border: "none",
                          padding: "1%",
                          paddingLeft: "10%",
                          paddingRight: "10%",
                          borderRadius: "5px",
                        }}
                        onClick={() => deleteMakanan(food.id)}
                      >
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          {list.length == 0 ? (
        <p>Makanan dan minuman tidak tersedia</p>
      ) : (
        <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          pageCount={pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={3}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      )}
        </>
      ) : (
        <></>
      )}

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton style={{ backgroundColor: "#5F9DF7" }}>
          <Modal.Title>Add Admin</Modal.Title>
        </Modal.Header>
        <Modal.Body className="form">
          <Form onSubmit={addList}>
            <div className="mb-3">
              <Form.Label>
                <strong>Gambar</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                {/* //value = nilai yang di inputkan */}
                <Form.Control
                  placeholder="Masukan Gambar"
                  onChange={(e) => setGambar(e.target.files[0])}
                  type="file"
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Nama Makanann</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukan Nama Makanan"
                  value={namaMakanans}
                  onChange={(e) => setnamaMakanans(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukan Deskripsi Makanan"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Harga</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="number"
                  value={harga}
                  onChange={(e) => setHarga(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <Button
              variant="danger"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
