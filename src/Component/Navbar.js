import React from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Navbar() {
  const history = useHistory();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      timer: 15000,
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Logout!", "Berhasil Logout", "success");
        window.location.reload();
      }
    });
    localStorage.clear();
    //Untuk munuju page selanjutnya
    history.pushState("/login");
  };

  return (
    <div>
      <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
          <strong>
            <h2>Warung Online</h2>
          </strong>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a
                  class="nav-link active"
                  aria-current="page"
                  href="/dashboard"
                >
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="/cart">Cart</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="/pofile">Profil</a>
              </li>
              {localStorage.getItem("id") !== null ? (
                    <>
              <li class="nav-item">
                <a class="nav-link" className="btn hover" onClick={logout}>
                  Logout
                </a>
              </li>
              </>
                  ) : (
              <li class="nav-item">
                <a class="nav-link" href="/login">
                  Login
                </a>
              </li>
                  )}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
