import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import "../Style/Card.css";
import AOS from "aos";
import ReactPaginate from "react-paginate";

export default function Card() {
  //function AOS
  AOS.init({ duration: 650, once: true });

  // const [makanan, setMakanan] = useState([]);
  const [list, setList] = useState([]);
  const [namaMakanans, setnamaMakanans] = useState("");
  const [pages, setPages] = useState(0);

  const getAll = async (idx) => {
    await axios
      .get(`http://localhost:5000/product/all?query=${namaMakanans}&page=${idx}`)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setList(
          res.data.data.content.map((item) => ({
            ...item,
            qty: 1,
          }))
        );
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [listData, setLisData] = useState([]);
  
  let color = "green";

  const plusquantityProduct = (idx) => {
    const listProduct = [...list];
    if (listProduct[idx].qty >= 45) {
      return;
    }
    listProduct[idx].qty++;
    setList(listProduct);
  };

  const minusquantityProduct = (idx) => {
    const listProduct = [...list];
    listProduct[idx].qty--;
    if (listProduct[idx].qty < 1) {
      listProduct[idx].qty = 1;
      return;
    }
    setList(listProduct);
  };

  useEffect(() => {
    getAll(0);
  }, []);

  // const addKranjang = async (food) => {
  //   await axios.post("http://localhost:8000/keranjang/", food);
  //   Swal.fire("Succes!", "Berhasil Di Masukan Keranjang", "success")
  //     .then(() => {
  //       // window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan " + error);
  //     });
  // };

  const addKranjang = async(food) => {
    console.log(food);
    await axios.post("http://localhost:5000/cart", {
      "productId": food.id,
      "qty": food.qty,
      "userId": `${localStorage.getItem("id")}`,
    });
    Swal.fire("Succes!", "Berhasil di masukan ke keranjang!", "success")
  };

  // const [items, setItems] = useState([]);
  // const [pageCount, setPageCount] = useState(0);

  // let limit = 2;

  // useEffect(() => {
  //   const getCommets = async () => {
  //     const res = await fetch(
  //       `localhost:5000/product/all?page=0&limit=${limit}`
  //     )
  //     const data = await res.json();
  //     const total = res.headers.get('x-total-count');
  //     setPageCount(Math.ceil(total/limit));
  //     // console.log(Math.ceil(total/1));
  //     setItems(data);
  //   }
  //   getCommets();
  // }, [])

  // console.log(items);

  // const handlePageClick = async (data) => {
  //   console.log(data.selected.content);

  //   let currentPage = data.selected.content + 1

  //   const commentsFormServer = await fetchComments(currentPage);

  //   setItems(commentsFormServer);
  // };

  // const fetchComments = async (currentPage) => {
  //   const res = await fetch(
  //     `localhost:5000/product/all?page=${currentPage}`
  //   )
  //   const data =await res.json();
  //   return data;
  // }

  return (
    <div>
      <div className="menu">
        <h3 className="h31">Daftar Menu Yang Dapat DiBeli</h3>
        <br />
        <form className="d-flex search1" role="search">
            <input
              class="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={namaMakanans}
              onChange={(e) => setnamaMakanans(e.target.value)}
            />
            <button
              class="btn btn-outline-success"
              type="button"
              onClick={() => getAll(0)}
            >
              Search
            </button>
          </form>
          <br />
        <div className="card1 card2">
          <div className="row gy-5 ct">
            {list.map((food,idx) => {
              return (
                <div key={food.id} className="col-4">
                  <div
                    data-aos="zoom-in-down"
                    className="card"
                  >
                    <img
                      src={food.gambar}
                      className="card-img-top gmb12"
                      alt="..."
                    />
                    <div className="card-body">
                      <h5 className="card-title">{food.namaMakanans}</h5>
                      <p
                        className="card-text"
                        style={{
                          height: "130px",
                          overflow: "auto",
                          textAlign: "justify",
                        }}
                      >
                        {food.deskripsi}
                      </p>
                    </div>
                    <div style={{ backgroundColor: "#98A8F8" }}>
                      <ul className="list-group list-group-flush">
                        <li
                          className="list-group-item"
                          style={{ backgroundColor: "#EEEEEE" }}
                        >
                          Rp.{food.harga}
                        </li>
                      </ul>
                    </div>
                    {localStorage.getItem("id") !== null ? (
                      <div className="card-body">
                        <div
                          style={{
                            display: "flex",
                            gap: "20px",
                            marginLeft: "40px",
                          }}
                        >
                          <button
                            className={`input-group-text ${food.qty<=1?"grey":"green"}`}
                            onClick={() => minusquantityProduct(idx)}
                          >
                            -
                          </button>
                          <div className="from-control text-center">
                            {food.qty}
                          </div>
                          <button
                            className={`input-group-text ${food.qty>=45?"grey":"green"}`}
                            onClick={() => plusquantityProduct(idx)} 
                          >
                            +
                          </button>
                          <button
                            type="button"
                            className="btn btn-success"
                            onClick={() => addKranjang(food)} 
                          >
                            Beli
                          </button>
                        </div>
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        
      {list.length == 0 ? (
        <p>Makanan dan minuman tidak tersedia</p>
      ) : (
        <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          pageCount={pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={3}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
        )}
      </div>
    </div>
  );
}
