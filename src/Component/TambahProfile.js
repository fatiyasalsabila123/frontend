import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const TambahProfile = () => {
  const [profile, setProfile] = useState([]);
  const [show, setShow] = useState(false);


    const [photoProfile, setPhotoProfile] = useState("");
    const [nama, setNama] = useState("");
    const [alamat, setAlamat] = useState("");
    const [email, setEmail] = useState("");
    const [noHp, setNoHp] = useState("");
    const [password, setPassword] = useState("");
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    const history = useHistory();

    useEffect(() => {
      axios.get(
          "http://localhost:5000/users/" +
            localStorage.getItem("id"))
            .then((response) => {
              const profil = response.data.data;
              setPhotoProfile(profile.photoProfile);
              setNama(profil.nama);
              setAlamat(profil.alamat);
              setEmail(profil.email);
              setNoHp(profil.noHp);
              setPassword(profil.password);
            })
        .catch((error) => {
          alert("Terjadi Kesalahan " + error);
        });
    },[]);

    const submitActionHandler = async (e) => {
      e.preventDefault();
      await Swal.fire({
        title: "Apakah Anda Yakin Untuk Di Edit?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios.put("http://localhost:5000/users/"+ localStorage.getItem("id"), {
            photoProfile: photoProfile,
            nama: nama,
            alamat: alamat,
            email: email,
            noHp: noHp,
            password: password,
          })
        }
      })
      .then(() => {
        Swal.fire({
          title: "Berhasil",
          icon: "success",
          showConfirmButton : false,
          timer: 1500
        })
        history.push("/profile");
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
        console.log(error);
      });
    }
    
    return (
    <div>
      {/* <div className="edit">
        <div className="edit mx-5">
          <div className="container my-5">
            <Form onSubmit={submitActionHandler}>
              <div className="name mb-3">
                <Form.Label>
                  <strong>Gambar</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="Gambar"
                    value={profile}
                    onChange={(e) => setProfile(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="place-of-birth mb-3">
                <Form.Label>
                  <strong>Username</strong>
                </Form.Label>
                <InputGroup className="flex gap-3">
                  <Form.Control
                    placeholder="Usernamae"
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                    required
                    />
                </InputGroup>
              </div>

              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Alamat</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                  placeholder="Alamat"
                    type="text"
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                    required
                    />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>No Handphone</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="number"
                    value={noHp}
                    onChange={(e) => setNoHp(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="password"
                    value={email}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="d-flex justify-content-end align-item-center mt-2">
                <button className="buton btn" type="submit">
                  Save
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div> */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton style={{ backgroundColor: "#5F9DF7" }}>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body className="form">
          <Form onSubmit={TambahProfile}>
            <div className="mb-3">
              <Form.Label>
                <strong>Username</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukan Nama Makanan"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukan Deskripsi Makanan"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>No Handphone</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="number"
                  value={noHp}
                  onChange={(e) => setNoHp(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <Button
              variant="danger"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 buton-btl"
              onClick={handleClose}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  )
};
export default TambahProfile;
  
  