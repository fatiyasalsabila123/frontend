import React from 'react'
import "../Style/List.css"
import AOS from "aos";

export default function List() {
  AOS.init({duration: 650, once:true});
  return (
    <div>
        <div>
            <h3 data-aos="zoom-in" className='h9'>Menu Yang Tersedia</h3>
            <div className='list'>
              <div data-aos="zoom-out-right" className='block'>
            <h4 className='h3' >Makanan Ringan</h4>
            <li><a style={{textDecoration: "none"}}  href="/mie">Mie</a> </li>
            <li><a style={{textDecoration: "none"}} href="/roti">Roti</a> </li>
            <li><a style={{textDecoration: "none"}} href="/keripik">Kripik</a> </li>
            <li><a style={{textDecoration: "none"}} href="/snack">Snack</a></li>
            </div>
            <div data-aos="zoom-in" className='block'>
            <h4 className='h3' >Minuman</h4>
            <li><a style={{textDecoration: "none"}} href="/kopi">Kopi</a></li>
            <li><a style={{textDecoration: "none"}} href="/jusbuah">Jus Buah</a></li>
            <li><a style={{textDecoration: "none"}} href="/teh">Teh</a></li>
            <li><a style={{textDecoration: "none"}} href="/jamu">Jamu</a></li>
            </div>
            <div data-aos="zoom-out-left" className='block'>
            <h4 className='h3' >Makanan Berat</h4>
            <li><a style={{textDecoration: "none"}} href="/nasigoreng">Nasi Goreng</a></li>
            <li><a style={{textDecoration: "none"}} href="soto">Soto</a></li>
            <li><a style={{textDecoration: "none"}} href="sate">Sate</a></li>
            <li><a style={{textDecoration: "none"}} href="rendang">rendang</a></li>
            </div>
            </div>
        </div>
        {/* <nav aria-label="Page navigation example" style={{marginLeft:"45%", marginTop:"5%"}}>
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="/list">1</a></li>
    <li class="page-item"><a class="page-link" href="/tabel">2</a></li>
    <li class="page-item"><a class="page-link" href="/card">3</a></li>
    <li class="page-item"><a class="page-link" href="/dashboard">4</a></li>
  </ul>
</nav> */}
    </div>
  )
}
