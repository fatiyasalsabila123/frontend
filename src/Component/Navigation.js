import axios from "axios";
import React, { useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../Style/Navigation.css";
import logo  from "../Component/logo.png";

export default function Navigation() {

  const history = useHistory();

  const logout = () => {
    localStorage.clear();
    history.push("/dashboard");
    window.location.reload();
  };
  return (
    <div className="navbarr">
      <div className="bar" style={{ backgroundColor: "#" }}>
        <Navbar >
          <Container>
            <img
            className="gambar"
              src={logo}
              alt=""
            />
            <Navbar.Brand className="warung"
             
            >
              Warung Online
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <div className="home">
                  <button type="button" className="bg-transparent border-0">
                    <a
                      style={{
                        textDecoration: "none",
                        color: "black",
                        fontWeight: "bold",
                        border: "none",
                      }}
                      href="/dashboard"
                      className="hover"
                    >
                      Home
                    </a>
                  </button>

                  {/* //{localStorage.getItem("id") !== null ? ( */}

                  {localStorage.getItem("role") !== null ? (
                    <>
                      <button
                        className="bg-transparent"
                        type="button"
                        style={{ border: "none" }}
                      >
                        <a
                          className="btn hover"
                          onClick={logout}
                        >
                          Logout
                        </a>
                      </button>
                    </>
                  ) : (
                    <button
                      className="nav-item float-right border-0 bg-transparent"
                      style={{  fontWeight: "bold" }}
                    >
                      <a
                        className="btn hover"
                        href="/login"
                      >
                        Login
                      </a>
                    </button>
                  )}
                  
                  <button
                    className="bg-transparent"
                    type="button"
                    style={{
                      
                      border: "none",
                      fontWeight: "bold",
                    }}
                  >
                    <a
                      style={{
                        textDecoration: "none",
                        color: "black",
                       
                      }}
                      href="/cart"
                      className="hover"
                    >
                      {" "}
                      Cart
                    </a>
                  </button>
                  
              
                </div>
                  <button
                    className="bg-transparent profile12"
                    type="button"
                    style={{
                      
                      border: "none",
                      fontWeight: "bold",
                    }}
                  >
                    <a
                      style={{
                        textDecoration: "none",
                        color: "black",
                       
                      }}
                      href="/profile"
                      className="hover"
                    >
                      {" "}
                      Profile
                    </a>
                  </button>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    </div>
  );
}
