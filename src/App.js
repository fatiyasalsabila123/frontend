import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Register from './Pages/Register';
import Login from './Pages/Login';
import Dashboard from './Component/Dashboard';
import Cart from './Component/Cart';
import Kopi from './Menu/Kopi';
import MakananRingan from './Menu/MakananRingan';
import Mie from './Menu/Mie';
import LoginPembeli from './Pages/LoginPembeli';
import Roti from './Menu/Roti';
import JusBuah from './Menu/JusBuah';
import Teh from './Menu/Teh';
import Jamu from './Menu/Jamu';
import NasiGoreng from './Menu/NasiGoreng';
import Soto from './Menu/Soto';
import Sate from './Menu/Sate';
import Rendang from './Menu/Rendang';
import Snack from './Menu/Snack';
import Edit from './Pages/Edit';
import Tabel from './Component/Tabel';
import Card from './Component/Card';
import ProfileUser from './Pages/ProfileUser';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={Register} exact />
            <Route path="/login" component={Login} exact/>
            <Route path="/Dashboard" component={Dashboard} exact/>
            <Route path="/cart" component={Cart} exact/>
            <Route path="/kopi" component={Kopi} exact/>
            <Route path="/makananringan" component={MakananRingan} exact/>
            <Route path="/mie" component={Mie} exact/>
            <Route path="/loginpembeli" component={LoginPembeli} exact/>
            <Route path="/keripik" component={MakananRingan} exact/>
            <Route path="/jusbuah" component={JusBuah} exact/>
            <Route path="/teh" component={Teh} exact/>
            <Route path="/jamu" component={Jamu} exact/>
            <Route path="/nasigoreng" component={NasiGoreng} exact/>
            <Route path="/soto" component={Soto} exact/>
            <Route path="/sate" component={Sate} exact/>
            <Route path="/rendang" component={Rendang} exact/>
            <Route path="/snack" component={Snack} exact/>
            <Route path="/roti" component={Roti} exact/>
            <Route path="/edit/:id" component={Edit} exact/>
            <Route path="/tabel" component={Tabel} exact/>
            <Route path="/card" component={Card} exact/>
            <Route path="/profile" component={ProfileUser} exact/>
\          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
