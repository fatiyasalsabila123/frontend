import React, { useEffect, useState } from "react";
import axios from "axios";
import "../Style/Card.css";
import "../Style/Back.css";
import AOS from "aos";
import Navigation from "../Component/Navigation";
import Swal from "sweetalert2";
import "../Style/Jamu.css"

export default function Jamu() {
  AOS.init({ duration: 650, once: true });
  const [makanan, setMakanan] = useState([]);

  const getAll = () => {
    axios
      .get("http://localhost:8000/jamu")
      .then((res) => {
        setMakanan(
          res.data.map((item) => ({
            ...item,
            qty: 1,
          }))
        );
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const plusquantityProduct = (idx) => {
    const listProduct = [...makanan];
    if (listProduct[idx].qty >= 45) {
      return;
    }
    listProduct[idx].qty++;
    setMakanan(listProduct);
  };

  const minusquantityProduct = (idx) => {
    const listProduct = [...makanan];
    listProduct[idx].qty--;
    if (listProduct[idx].qty < 1) {
      listProduct[idx].qty = 1;
      return;
    }
    setMakanan(listProduct);
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const addKranjang = async (minuman) => {
    await axios.post("http://localhost:8000/keranjang", minuman);
    Swal.fire("Succes!", "Berhasil Di Tambahkan Di Keranjang", "success")
      .then(() => {
        // window.location.reload();
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  };

  return (
    <div className="back">
      {/* Navbar */}
      <Navigation />
      <div  >
        <h1 data-aos="zoom-out" className="hm">Jamu</h1>
        <div className="card12" >
          <div className="row gy-5">
            {makanan.map((minuman,idx) => {
              return (
                <div className="col-3">
                  <div data-aos="zoom-in" className="card" style={{ width: "18rem" }}>
                    <img
                      src={minuman.gambar}
                      className="card-img-top"
                      style={{ height: "200px" }}
                      alt="..."
                    />
                    <div className="card-body">
                      <h5 className="card-title">{minuman.namaMakanans}</h5>
                      <p
                        className="card-text"
                        style={{ height: "130px", overflow: "auto" }}
                      >
                        {minuman.deskripsi}
                      </p>
                    </div>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">{minuman.harga}</li>
                    </ul>
                    {localStorage.getItem("id") !== null ? (
                      <div className="card-body">
                         <button
                            className={`input-group-text ${minuman.qty<=1?"grey":"green"}`}
                            onClick={() => minusquantityProduct(idx)}
                          >
                            -
                          </button>
                          <div className="from-control text-center">
                            {minuman.qty}
                          </div>
                          <button
                            className={`input-group-text ${minuman.qty>=45?"grey":"green"}`}
                            onClick={() => plusquantityProduct(idx)} 
                          >
                            +
                          </button>
                        <button
                          type="button"
                          className="btn btn-success"
                          onClick={() => addKranjang(minuman)}
                        >
                          Beli
                        </button>
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
