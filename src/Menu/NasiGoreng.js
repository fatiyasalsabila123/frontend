import React, { useEffect, useState } from 'react'
import axios from 'axios';
import "../Style/Card.css"
import AOS from "aos";
import Navigation from '../Component/Navigation';
import Swal from 'sweetalert2';
import "../Style/Jamu.css"
import "../Style/Jamu.css"


export default function NasiGoreng() {
  AOS.init({duration: 650, once:true});
    const [makanan, setMakanan] = useState([]);
  
    const getAll = () => {
      axios
        .get("http://localhost:8000/nasiGoreng")
        .then((res) => {
          setMakanan(res.data);
        })
        .catch((error) => {
          alert("Terjadi Kesalahan" + error);
        });
    }

    useEffect(() => {
        getAll();
      }, []);

      const addKranjang = async (minuman) => {
        await axios.post("http://localhost:8000/keranjang/",  minuman);
        Swal.fire("Succes!", "Berhasil Di Tambahkan Di Keranjang", "success")
          .then(() => {
            // window.location.reload();
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
        }

  return (
    <div> 
      <Navigation/>
      <div data-aos="zoom-out" className="hm">Nasi Goreng</div>
    <div className='card12' data-aos="zoom-in" >
        <div className="row gy-5">
          {makanan.map((minuman) => {
            return (
              <div className="col-3">
                <div data-aos="zoom-in" className="card" style={{ width: "18rem" }}>
                  <img src={minuman.gambar} className="card-img-top" style={{height: "200px"}} alt="..." />
                  <div className="card-body">
                    <h5 className="card-title">{minuman.namaMakanans}</h5>
                    <p className="card-text"style={{height: "130px" ,overflow: "auto"}}>{minuman.deskripsi}</p>
                  </div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{minuman.harga}</li>
                  </ul>
                  {localStorage.getItem("id") !== null ? (
                  <div className="card-body">
                    <button type="button" className="btn btn-success"  onClick={() => addKranjang(minuman)} >
                      Beli
                    </button>
                  </div>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            );
          })}
        </div>
    </div>
    </div>
  )
}
