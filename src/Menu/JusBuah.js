import React, { useEffect, useState } from 'react'
import axios from 'axios';
import "../Style/Card.css"
import AOS from "aos";
import Navigation from '../Component/Navigation';
import Swal from 'sweetalert2';
import "../Style/Jamu.css"

export default function JusBuah() {
    const [makanan, setMakanan] = useState([]);
  
    const getAll = () => {
      axios
        .get("http://localhost:8000/jusBuah")
        .then((res) => {
          setMakanan(res.data);
        })
        .catch((error) => {
          alert("Terjadi Kesalahan" + error);
        });
    }

    useEffect(() => {
        getAll();
      }, []);

      const addKranjang = async (food) => {
        await axios.post("http://localhost:8000/keranjang/",  food);
        Swal.fire("Succes!", "Berhasil Di Tambahkan Di Keranjang", "success")
          .then(() => {
            // window.location.reload();
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
        }

      AOS.init({duration: 650, once:true});
  return (
    <div>
      <Navigation/>
      <div data-aos="zoom-out" className="hm">Jus Buah</div>
    <div className='card12' data-aos="zoom-in" >
        <div className="row gy-5">
          {makanan.map((food) => {
            return (
              <div className="col-3">
                <div data-aos="zoom-in" className="card" style={{ width: "18rem" }}>
                  <img src={food.gambar} className="card-img-top" style={{height: "200px"}} alt="..." />
                  <div className="card-body">
                    <h5 className="card-title">{food.namaMakanans}</h5>
                    <p className="card-text" style={{height: "130px" ,overflow: "auto"}}>{food.deskripsi}</p>
                  </div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{food.harga}</li>
                  </ul>
                  {localStorage.getItem("id") !== null ? (
                  <div className="card-body">
                    <button type="button" className="btn btn-success" onClick={() => addKranjang(food)} >
                      Beli
                    </button>
                  </div>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            );
          })}
        </div>
    </div>
    </div>
  )
}
